(*
 * libssh OCaml bindings
 * Copyright (C) 2019 Pino Toscano <ptoscano@redhat.com>
 *
 * You are free to copy this file, modify it in any way, consider it being
 * public domain. This does not apply to the rest of the library though,
 * but it is allowed to cut-and-paste working code from this file to any
 * license of program.
 *)

let () =
  (* Generate a new ED25519 private key file *)
  let key = Libssh.ssh_pki_generate Libssh.SSH_KEYTYPE_ED25519 0 in

  (* Write it to a file testkey in the current dirrectory *)
  key#export_privkey_file "testkey"
