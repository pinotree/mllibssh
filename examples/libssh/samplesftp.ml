(*
 * libssh OCaml bindings
 * Copyright (C) 2019 Pino Toscano <ptoscano@redhat.com>
 *
 * You are free to copy this file, modify it in any way, consider it being
 * public domain. This does not apply to the rest of the library though,
 * but it is allowed to cut-and-paste working code from this file to any
 * license of program.
 *)

open Printf

let optstr = function
  | None -> "(none)"
  | Some str -> str

let string_of_sftp_statvfs_flag = function
  | Libssh.SSH_FXE_STATVFS_ST_RDONLY -> "rdonly"
  | SSH_FXE_STATVFS_ST_NOSUID -> "nosuid"

let do_sftp session =
  let sftp = new Libssh.sftp session in

  printf "Additional SFTP extensions provided by the server:\n";
  let extensions = sftp#extensions () in
  Array.iter (
    fun (name, data) ->
      printf "\t%s, version: %s\n" name data
  ) extensions;

  (* test symlink and readlink *)
  sftp#symlink "/tmp/this_is_the_link" "/tmp/sftp_symlink_test";
  let lnk = sftp#readlink "/tmp/sftp_symlink_test" in
  printf "readlink /tmp/sftp_symlink_test: %s\n" lnk;

  sftp#unlink "/tmp/sftp_symlink_test";

  if sftp#extension_supported "statvfs@openssh.com" "2" then (
    let sftpstatvfs = sftp#statvfs "/tmp" in

    printf "sftp statvfs:\n\
            \tfile system block size: %Lu\n\
            \tfundamental fs block size: %Lu\n\
            \tnumber of blocks (unit f_frsize): %Lu\n\
            \tfree blocks in file system: %Lu\n\
            \tfree blocks for non-root: %Lu\n\
            \ttotal file inodes: %Lu\n\
            \tfree file inodes: %Lu\n\
            \tfree file inodes for to non-root: %Lu\n\
            \tfile system id: %Lu\n\
            \tf_flag values: %s\n\
            \tmaximum filename length: %Lu\n"
           sftpstatvfs.Libssh.f_bsize
           sftpstatvfs.Libssh.f_frsize
           sftpstatvfs.Libssh.f_blocks
           sftpstatvfs.Libssh.f_bfree
           sftpstatvfs.Libssh.f_bavail
           sftpstatvfs.Libssh.f_files
           sftpstatvfs.Libssh.f_ffree
           sftpstatvfs.Libssh.f_favail
           sftpstatvfs.Libssh.f_fsid
           (String.concat "," (List.map string_of_sftp_statvfs_flag sftpstatvfs.Libssh.f_flag))
           sftpstatvfs.Libssh.f_namemax;
  );

  (* the connection is made *)
  (* opening a directory *)
  let dir = sftp#opendir "./" in

  (* reading the whole directory, file by file *)
  try
    while true do
      let file = dir#readdir () in
      printf "%30s(%.8o) : %s(%.5d) %s(%.5d) : %.10Lu bytes\n"
             (optstr file.Libssh.name)
             file.Libssh.permissions
             (optstr file.Libssh.owner)
             file.Libssh.uid
             (optstr file.Libssh.group)
             file.Libssh.gid
             file.Libssh.size;
    done
  with End_of_file -> ();

  (* this will open a file and copy it into your /home directory *)
  (* the small buffer size was intended to stress the library. of course, you
   * can use a buffer till 20kbytes without problem *)

  let fichier = sftp#open_file "/usr/bin/ssh" [Libssh.O_RDONLY] 0 in

  (* open a file for writing... *)
  let to_ = sftp#open_file "ssh-copy" [Libssh.O_WRONLY; Libssh.O_CREAT] 0o700 in

  let data = Bytes.create 65536 in
  try
    while true do
      let read = fichier#read data 0 4096 in
      ignore (to_#write data 0 read);
    done;
  with End_of_file -> ();

  printf "finished\n";

  printf "fichiers ferm\n";
  let to_ = sftp#open_file "/tmp/grosfichier" [Libssh.O_WRONLY; Libssh.O_CREAT] 0o644 in

  for i = 1 to 1000 do
    let len = to_#write data 0 (Bytes.length data) in
    printf "wrote %d bytes\n" len
  done;

  (* close the sftp session *)
  printf "sftp session terminated\n"

let opts () =
  let verbosity = ref Libssh.SSH_LOG_NONE in
  let increase_verbosity () =
    verbosity :=
      match !verbosity with
      | Libssh.SSH_LOG_NONE -> Libssh.SSH_LOG_WARN
      | SSH_LOG_WARN -> Libssh.SSH_LOG_INFO
      | SSH_LOG_INFO -> Libssh.SSH_LOG_DEBUG
      | SSH_LOG_DEBUG -> Libssh.SSH_LOG_TRACE
      | SSH_LOG_TRACE -> Libssh.SSH_LOG_TRACE
  in
  let args = Arg.align [
    "-v", Arg.Unit increase_verbosity, " increase log verbosity";
  ] in
  let usage_msg = sprintf "Usage : %s [-v] remotehost\n\
                           sample sftp test client - libssh-%s\n\
                           Options :"
                          Sys.argv.(0) (Libssh.libssh_version_string ()) in
  let destination = ref None in
  let anon_fun arg =
    match !destination with
    | None -> destination := Some arg
    | Some _ ->
      Arg.usage args usage_msg;
      exit 1
  in
  Arg.parse args anon_fun usage_msg;
  let destination =
    match !destination with
    | None ->
      Arg.usage args usage_msg;
      exit 1
    | Some destination -> destination in
  destination, !verbosity

let () =
  let destination, verbosity = opts () in

  let session = Connect_ssh.connect_ssh ~verbosity destination in

  do_sftp session
