(*
 * libssh OCaml bindings
 * Copyright (C) 2019 Pino Toscano <ptoscano@redhat.com>
 *
 * You are free to copy this file, modify it in any way, consider it being
 * public domain. This does not apply to the rest of the library though,
 * but it is allowed to cut-and-paste working code from this file to any
 * license of program.
 *)

let () =
  let session = Connect_ssh.connect_ssh "localhost" in

  let channel = new Libssh.channel session in
  channel#open_session ();
  channel#request_exec "lsof";

  let data = Bytes.create 256 in
  try
    while true do
      let read = channel#read data 0 (Bytes.length data) in
      print_bytes (Bytes.sub data 0 read);
    done;
  with End_of_file -> ()
