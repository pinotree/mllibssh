(*
 * libssh OCaml bindings
 * Copyright (C) 2019 Pino Toscano <ptoscano@redhat.com>
 *
 * You are free to copy this file, modify it in any way, consider it being
 * public domain. This does not apply to the rest of the library though,
 * but it is allowed to cut-and-paste working code from this file to any
 * license of program.
 *)

val authenticate_console : Libssh.ssh -> Libssh.ssh_auth_code
