(*
 * libssh OCaml bindings
 * Copyright (C) 2019 Pino Toscano <ptoscano@redhat.com>
 *
 * You are free to copy this file, modify it in any way, consider it being
 * public domain. This does not apply to the rest of the library though,
 * but it is allowed to cut-and-paste working code from this file to any
 * license of program.
 *)

open Printf

let createcommand =
  "rm -fr /tmp/libssh_tests && mkdir /tmp/libssh_tests && \
   cd /tmp/libssh_tests && date > a && date > b && mkdir c && date > d"

let opts () =
  let verbosity = ref Libssh.SSH_LOG_NONE in
  let increase_verbosity () =
    verbosity :=
      match !verbosity with
      | Libssh.SSH_LOG_NONE -> Libssh.SSH_LOG_WARN
      | SSH_LOG_WARN -> Libssh.SSH_LOG_INFO
      | SSH_LOG_INFO -> Libssh.SSH_LOG_DEBUG
      | SSH_LOG_DEBUG -> Libssh.SSH_LOG_TRACE
      | SSH_LOG_TRACE -> Libssh.SSH_LOG_TRACE
  in
  let args = Arg.align [
    "-v", Arg.Unit increase_verbosity, " increase log verbosity";
  ] in
  let usage_msg = sprintf "Usage : %s [-v] remotehost\n\
                           sample tiny scp downloader client - libssh-%s\n\
                           This program will create files in /tmp and try to fetch them\n\
                           Options :"
                          Sys.argv.(0) (Libssh.libssh_version_string ()) in
  let host = ref None in
  let anon_fun arg =
    match !host with
    | None -> host := Some arg
    | Some _ ->
      Arg.usage args usage_msg;
      exit 1
  in
  Arg.parse args anon_fun usage_msg;
  let host =
    match !host with
    | None ->
      Arg.usage args usage_msg;
      exit 1
    | Some host -> host in
  host, !verbosity

let create_files session =
  let channel = new Libssh.channel session in
  channel#open_session ();
  channel#request_exec createcommand;
  let data = Bytes.create 1 in
  try
    while true do
      let read = channel#read ~is_stderr:true data 0 (Bytes.length data) in
      print_bytes (Bytes.sub data 0 read);
    done;
  with End_of_file | Libssh.LibsshError _ -> ()

let fetch_files session =
  let scp = new Libssh.scp session ~recursive:true Libssh.SSH_SCP_READ "/tmp/libssh_tests/*" in
  printf "Trying to download 3 files (a,b,d) and 1 directory (c)\n";
  try
    while true do
      let r = scp#pull_request () in
      match r with
      | SSH_SCP_REQUEST_NEWFILE ->
        let size = scp#request_get_size () in
        let filename = scp#request_get_filename () in
        let mode = scp#request_get_permissions () in
        printf "downloading file %s, size %Ld, perms 0%o\n" filename size mode;
        scp#accept_request ();
        let data = Bytes.create 16384 in
        ignore (scp#read data 0 (Bytes.length data));
        printf "done\n"
      | SSH_SCP_REQUEST_WARNING ->
        fprintf stderr "Warning: %s\n" (scp#request_get_warning ())
      | SSH_SCP_REQUEST_NEWDIR ->
        let filename = scp#request_get_filename () in
        let mode = scp#request_get_permissions () in
        printf "downloading directory %s, perms 0%o\n" filename mode;
        scp#accept_request ()
      | SSH_SCP_REQUEST_ENDDIR ->
        printf "End of directory\n"
    done;
  with End_of_file ->
    printf "End of requests\n"

let () =
  let host, verbosity = opts () in
  let session = Connect_ssh.connect_ssh ~verbosity host in
  create_files session;
  fetch_files session
