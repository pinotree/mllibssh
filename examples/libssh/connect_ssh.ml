(*
 * libssh OCaml bindings
 * Copyright (C) 2019 Pino Toscano <ptoscano@redhat.com>
 *
 * You are free to copy this file, modify it in any way, consider it being
 * public domain. This does not apply to the rest of the library though,
 * but it is allowed to cut-and-paste working code from this file to any
 * license of program.
 *)

open Printf

let connect_ssh ?user ?verbosity host =
  let ssh = new Libssh.ssh () in

  (match user with
  | None -> ()
  | Some user -> ssh#options_set (Libssh.SSH_OPTIONS_USER user)
  );
  ssh#options_set (Libssh.SSH_OPTIONS_HOST host);
  (match verbosity with
  | None -> ()
  | Some v -> ssh#options_set (Libssh.SSH_OPTIONS_LOG_VERBOSITY v)
  );

  ssh#connect ();
  Knownhosts.verify_knownhost ssh;
  let auth = Authentication.authenticate_console ssh in
  (match auth with
  | Libssh.SSH_AUTH_SUCCESS -> ()
  | Libssh.SSH_AUTH_DENIED ->
    fprintf stderr "Authentication failed\n";
    exit 1
  | _ ->
    fprintf stderr "Error while authenticating : %s\n"
      (snd (ssh#get_error ()));
    exit 1
  );

  ssh
