(*
 * libssh OCaml bindings
 * Copyright (C) 2019 Pino Toscano <ptoscano@redhat.com>
 *
 * You are free to copy this file, modify it in any way, consider it being
 * public domain. This does not apply to the rest of the library though,
 * but it is allowed to cut-and-paste working code from this file to any
 * license of program.
 *)

open Printf

let limit = 0x100000000_L

let () =
  let session = Connect_ssh.connect_ssh "localhost" in
  let channel = new Libssh.channel session in
  channel#open_session ();
  channel#request_exec "cat > /dev/null";
  let data = Bytes.create (1024*1024) in
  let total = ref 0_L in
  let lastshown = ref 4096_L in
  try
    while true do
      let rc = Int64.of_int (channel#write data 0 (Bytes.length data)) in
      total := Int64.add !total rc;
      if Int64.div !total 2_L >= !lastshown then (
        printf "written %Lx\n" !total;
        lastshown := !total;
      );
      if !total > limit then
        raise End_of_file;
    done;
  with End_of_file -> ();
  channel#send_eof ()
