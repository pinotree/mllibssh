(*
 * libssh OCaml bindings
 * Copyright (C) 2019 Pino Toscano <ptoscano@redhat.com>
 *
 * You are free to copy this file, modify it in any way, consider it being
 * public domain. This does not apply to the rest of the library though,
 * but it is allowed to cut-and-paste working code from this file to any
 * license of program.
 *)

open Printf

let print_hash hashtype hash =
  let fingerprint = Libssh.ssh_get_fingerprint_hash hashtype hash in
  fprintf stderr "%s\n" fingerprint

let verify_knownhost session =
  let srv_pubkey = session#get_server_publickey () in

  let hash = srv_pubkey#get_publickey_hash Libssh.SSH_PUBLICKEY_HASH_SHA256 in

  let state = session#is_known_server () in

  match state with
  | Libssh.SSH_KNOWN_HOSTS_OK ->
    () (* ok *)
  | Libssh.SSH_KNOWN_HOSTS_CHANGED ->
    fprintf stderr "Host key for server changed : server's one is now :\n";
    print_hash Libssh.SSH_PUBLICKEY_HASH_SHA256 hash;
    fprintf stderr "For security reason, connection will be stopped\n";
    exit 1
  | Libssh.SSH_KNOWN_HOSTS_OTHER ->
    fprintf stderr "The host key for this server was not found but an other type of key exists.\n";
    fprintf stderr "An attacker might change the default server key to confuse your client\
                    into thinking the key does not exist\n\
                    We advise you to rerun the client with -d or -r for more safety.\n";
    exit 1
  | Libssh.SSH_KNOWN_HOSTS_NOT_FOUND | Libssh.SSH_KNOWN_HOSTS_UNKNOWN ->
    if state = Libssh.SSH_KNOWN_HOSTS_NOT_FOUND then (
      fprintf stderr "Could not find known host file. If you accept the host key here,\n";
      fprintf stderr "the file will be automatically created.\n";
    );
    fprintf stderr
            "The server is unknown. Do you trust the host key (yes/no)?\n";
    print_hash Libssh.SSH_PUBLICKEY_HASH_SHA256 hash;

    let buf = String.lowercase_ascii (read_line ()) in
    if buf <> "yes" then
      exit 1;
    fprintf stderr "This new key will be written on disk for further usage. do you agree ?\n";
    let buf = String.lowercase_ascii (read_line ()) in
    if buf = "yes" then
      session#update_known_hosts ()
