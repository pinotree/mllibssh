(*
 * libssh OCaml bindings
 * Copyright (C) 2019 Pino Toscano <ptoscano@redhat.com>
 *
 * You are free to copy this file, modify it in any way, consider it being
 * public domain. This does not apply to the rest of the library though,
 * but it is allowed to cut-and-paste working code from this file to any
 * license of program.
 *)

open Printf

let read_line_noecho () =
  let old = Unix.tcgetattr Unix.stdin in
  let noecho = Unix.tcgetattr Unix.stdin in
  noecho.Unix.c_echo <- false;
  noecho.Unix.c_echonl <- true;
  Unix.tcsetattr Unix.stdin Unix.TCSAFLUSH noecho;
  let l = read_line () in
  Unix.tcsetattr Unix.stdin Unix.TCSAFLUSH old;
  l

let auth_keyfile session keyfile =
  let pubkey = keyfile ^ ".pub" in
  let key = Libssh.ssh_pki_import_pubkey_file pubkey in
  let rc = session#userauth_try_publickey key in
  if rc = Libssh.SSH_AUTH_SUCCESS then (
    let key = Libssh.ssh_pki_import_privkey_file keyfile in
    session#userauth_publickey key
  )
  else rc

let try_auth_publickey session =
  let rc = ref (session#userauth_publickey_auto ()) in
  if !rc <> Libssh.SSH_AUTH_ERROR && !rc <> Libssh.SSH_AUTH_SUCCESS then (
    printf "Automatic pubkey failed. \
            Do you want to try a specific key? (y/n)\n";
    let buf = String.lowercase_ascii (read_line ()) in
    if buf <> "" && (String.get buf 0) = 'y' then (
      printf "private key filename: ";
      let buf = read_line () in
      rc := auth_keyfile session buf;
      if !rc <> Libssh.SSH_AUTH_SUCCESS then
        fprintf stderr "failed with key\n";
    );
  );
  !rc

let try_auth_interactive session =
  let err = ref (session#userauth_kbdint ()) in
  while !err = Libssh.SSH_AUTH_INFO do
    let name = session#userauth_kbdint_getname () in
    let instruction = session#userauth_kbdint_getinstruction () in
    let prompts = session#userauth_kbdint_getprompts () in

    (match name with
    | None -> ()
    | Some str-> print_endline str
    );

    (match instruction with
    | None -> ()
    | Some str-> print_endline str
    );

    let answers = List.map (
      fun { Libssh.prompt; echo } ->
        print_string prompt;
        (if echo then read_line else read_line_noecho) ()
    ) prompts in
    session#userauth_kbdint_setanswers answers;
    err := session#userauth_kbdint ();
  done;
  !err

let try_auth_password session =
  print_string "Password: ";
  let pwd = read_line_noecho () in
  session#userauth_password pwd

let error session =
  fprintf stderr "Authentication failed: %s\n" (snd (session#get_error ()))

let authenticate_console session =
  (* Try to authenticate *)
  let rc = ref (session#userauth_none ()) in
  if !rc <> Libssh.SSH_AUTH_ERROR then (
    let methods = session#userauth_list () in
    let auths = [
      Libssh.SSH_AUTH_METHOD_PUBLICKEY, try_auth_publickey;
      Libssh.SSH_AUTH_METHOD_INTERACTIVE, try_auth_interactive;
      Libssh.SSH_AUTH_METHOD_PASSWORD, try_auth_password;
    ] in
    let rec loop = function
      | [] -> Libssh.SSH_AUTH_DENIED
      | (typ, fn) :: xs ->
        if List.mem typ methods then (
          match fn session with
          | Libssh.SSH_AUTH_ERROR as rc ->
            error session;
            rc
          | Libssh.SSH_AUTH_SUCCESS as rc -> rc
          | res -> loop xs
        ) else
          loop xs
    in
    rc := loop auths;
  );

  (match !rc with
  | Libssh.SSH_AUTH_SUCCESS ->
    let banner = session#get_issue_banner () in
    (match banner with
    | None -> ()
    | Some banner -> printf "%s\n" banner)
  | _ -> ()
  );

  !rc
