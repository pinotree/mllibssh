(*
 * libssh OCaml bindings
 * Copyright (C) 2019 Pino Toscano <ptoscano@redhat.com>
 *
 * You are free to copy this file, modify it in any way, consider it being
 * public domain. This does not apply to the rest of the library though,
 * but it is allowed to cut-and-paste working code from this file to any
 * license of program.
 *)

open Printf

let read_line_noecho () =
  let old = Unix.tcgetattr Unix.stdin in
  let noecho = Unix.tcgetattr Unix.stdin in
  noecho.Unix.c_echo <- false;
  noecho.Unix.c_echonl <- true;
  Unix.tcsetattr Unix.stdin Unix.TCSAFLUSH noecho;
  let l = read_line () in
  Unix.tcsetattr Unix.stdin Unix.TCSAFLUSH old;
  l

let auth_callback prompt echo _ =
  print_string prompt;
  Some ((if echo then read_line else read_line_noecho) ())

let string_of_ssh_keytype = function
  | Libssh.SSH_KEYTYPE_UNKNOWN -> "unknown"
  | SSH_KEYTYPE_DSS -> "dss"
  | SSH_KEYTYPE_RSA -> "rsa"
  | SSH_KEYTYPE_RSA1 -> "rsa1"
  | SSH_KEYTYPE_ECDSA -> "ecdsa"
  | SSH_KEYTYPE_ED25519 -> "ed25519"
  | SSH_KEYTYPE_DSS_CERT01 -> "dss-cert01"
  | SSH_KEYTYPE_RSA_CERT01 -> "rsa-cert01"
  | SSH_KEYTYPE_OTHER s -> s

let optstr = function
  | None -> "(none)"
  | Some str -> str

let fingerprints = [ Libssh.SSH_PUBLICKEY_HASH_MD5; Libssh.SSH_PUBLICKEY_HASH_SHA1; Libssh.SSH_PUBLICKEY_HASH_SHA256 ]

let () =
  if Array.length Sys.argv < 2 then (
    prerr_endline "Missing private key";
    exit 1;
  );
  let key = Libssh.ssh_pki_import_privkey_file ~auth:(Libssh.Callback auth_callback) Sys.argv.(1) in
  print_newline ();
  printf "type: %s\n%!" (string_of_ssh_keytype (key#typ ()));
  printf "fingerprints:\n%!";
  List.iter (
    fun fp ->
      let hash = key#get_publickey_hash fp in
      let str = Libssh.ssh_get_fingerprint_hash fp hash in
      printf "  %s\n%!" str;
  ) fingerprints;
  printf "is/has public? %B\n%!" (key#is_public ());
  printf "is private? %B\n%!" (key#is_private ());
  printf "ECDA name: %s\n%!" (optstr (key#ecdsa_name ()))
