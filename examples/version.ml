(*
 * libssh OCaml bindings
 * Copyright (C) 2019 Pino Toscano <ptoscano@redhat.com>
 *
 * You are free to copy this file, modify it in any way, consider it being
 * public domain. This does not apply to the rest of the library though,
 * but it is allowed to cut-and-paste working code from this file to any
 * license of program.
 *)

open Printf

let () =
  let major, minor, micro = Libssh.libssh_version () in
  let version_string = Libssh.libssh_version_string () in
  let copyright = Libssh.ssh_copyright () in
  printf "libssh build version: %d.%d.%d\n%!" major minor micro;
  printf "libssh runtime version: %s\n%!" version_string;
  printf "libssh copyright: %s\n%!" copyright
