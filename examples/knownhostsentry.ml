(*
 * libssh OCaml bindings
 * Copyright (C) 2019 Pino Toscano <ptoscano@redhat.com>
 *
 * You are free to copy this file, modify it in any way, consider it being
 * public domain. This does not apply to the rest of the library though,
 * but it is allowed to cut-and-paste working code from this file to any
 * license of program.
 *)

open Printf

let string_of_ssh_keytype = function
  | Libssh.SSH_KEYTYPE_UNKNOWN -> "unknown"
  | SSH_KEYTYPE_DSS -> "dss"
  | SSH_KEYTYPE_RSA -> "rsa"
  | SSH_KEYTYPE_RSA1 -> "rsa1"
  | SSH_KEYTYPE_ECDSA -> "ecdsa"
  | SSH_KEYTYPE_ED25519 -> "ed25519"
  | SSH_KEYTYPE_DSS_CERT01 -> "dss-cert01"
  | SSH_KEYTYPE_RSA_CERT01 -> "rsa-cert01"
  | SSH_KEYTYPE_OTHER s -> s

let optstr = function
  | None -> "(none)"
  | Some str -> str

let () =
  if Array.length Sys.argv < 3 then (
    prerr_endline "Missing hostname and line";
    exit 1;
  );
  let entry = Libssh.ssh_known_hosts_parse_line Sys.argv.(1) Sys.argv.(2) in
  printf "hostname: %s\n%!" entry.Libssh.hostname;
  printf "unparsed: %s\n%!" entry.Libssh.unparsed;
  printf "type: %s\n%!" (string_of_ssh_keytype (entry.Libssh.publickey#typ ()))
