mllibssh
========

mllibssh provides OCaml bindings for the libssh library.

libssh is a library implementing the SSHv2 protocol on client and
server side.  For more information, see <https://www.libssh.org>.

The API provided by mllibssh is mostly object-oriented, and based on
libssh 0.8.0.

Installation
------------

mlibssh uses [Dune][dune] as build system. The development files for
libssh are required, usually provided by a -dev/-devel package in
binary distributions. Also, `pkg-config` is required to detect and
link to libssh.

```shell
$ git clone https://gitlab.com/pinotree/mllibssh
$ cd mllibssh
mllibssh$ dune build
mllibssh$ dune install
```

There are few examples provided. They can be built using the
`@examples` target:

```shell
mllibssh$ dune build @examples
mllibssh$ _build/default/examples/version.exe
libssh build version: 0.9.3
libssh runtime version: 0.9.3/openssl/zlib
libssh copyright: 0.9.3 (c) 2003-2019 Aris Adamantiadis, Andreas Schneider and libssh contributors. Distributed under the LGPL, please refer to COPYING file for information about your rights
```

[dune]: https://dune.build/

Bugs & contributions
--------------------

Bugs can be reported to the issue tracking system. Similarly, merge
requests can be submitted too.
